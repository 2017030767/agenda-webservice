package com.example.agendaws.Objetos;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;

public class Device {
    public static final String getSecureId(Context context){
        @SuppressLint("HardwareIds") String id= Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return id;
    }
}
